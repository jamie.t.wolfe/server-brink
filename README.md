# Brink Dedicated Server Docker Container
This container provides a dedicated server for the game Brink.

## Getting Started
### Prerequisites
You will need to have Docker installed on your system in order to use this container.

### Running the Container
To run the container, use the following command:

```
docker run -d -p 27015:27015/udp -p 4976:4976/tcp -v /path/to/brinkserver.cfg:/home/brinkserver/base/brinkserver.cfg --name brink-dedicated-server openkitten/brink-server:latest
```
This will start a new container and expose ports 27015/udp and 4976/tcp, which are required for the game to function properly. The brinkserver.cfg file should be located on your host system and will be mounted into the container at runtime.

### Configuring the Server
To configure the server, you will need to create a brinkserver.cfg file on your host system and mount it into the container as described above. The contents of this file should be based on the example provided in the documentation, and should be modified as necessary for your specific needs.

Configuration can also be controlled with variables, the following are a few examples.
| Variable Name | Default Value | Description |
| --- | --- | --- |
| `BRINK_SERVER_NAME` | `Brink Server` | Name of the server displayed in the server list |
| `BRINK_ADMIN_NAME` | `Admin` | Name of the server admin |
| `BRINK_ADMIN_EMAIL` | `admin@example.com` | Email address of the server admin |
| `BRINK_ADMIN_IRC` | `irc.example.com` | IRC server for server admin |
| `BRINK_MOTD_1` | `Welcome to my Brink Server!` | Message of the day displayed on server connect |
| `BRINK_MOTD_2` | `Please be respectful to other players.` | Additional message of the day |
| `BRINK_MOTD_3` | `Have fun!` | Additional message of the day |
| `BRINK_MAX_PLAYERS` | `16` | Maximum number of players allowed on the server |
| `BRINK_MIN_PLAYERS` | `1` | Minimum number of players required to start a match |
| `BRINK_MAP_ROTATION` | `mp/aquarium, mp/ccity, mp/reactor, mp/refuel, mp/resort, mp/sectow, mp/shipyard, mp/terminal` | Comma-separated list of maps in rotation |
| `BRINK_SERVER_PASSWORD` | `password` | Password required to join the server |
| `BRINK_REMOTE_CONSOLE_PASSWORD` | `password` | Password required to access remote console |
| `BRINK_ALLOW_HIJACKING` | `1` | Allow other players to use the server for Co-Op mode |


### Connecting to the Server
Once the container is running, you should be able to connect to it using the game client. Simply search for the server by name or IP address, and connect as you would normally.

### Contributing
If you would like to contribute to this project, please feel free to submit a pull request or open an issue on the project's GitHub page.

### License
This project is licensed under the MIT License - see the LICENSE file for details.



