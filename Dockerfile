# Use the official Ubuntu 18.04 LTS image as the base image
FROM ubuntu:18.04

# Set environment variables for the server configuration
ENV SERVER_NAME="My Brink Server"
ENV SERVER_PORT="27015"
ENV SERVER_PORT_MASTER="27016"
ENV SERVER_PASSWORD=""
ENV MAX_PLAYERS="16"
ENV MIN_PLAYERS="2"
ENV MAX_TEAM_SIZE="8"
ENV TEAM_DAMAGE="Off"
ENV RANK_RESTRICTIONS="On"
ENV MAP_ROTATION=""
ENV FIXED_MAP_DURATION="Off"
ENV OVERTIME="On"
ENV GLOBAL_VOIP="Off"
ENV TEAM_VOIP="Off"
ENV BOTS="On"
ENV VOTING="Off"
ENV RULES_CONFIG="server_objective_standard_vs.cfg"

# Update the system and install necessary packages
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y wget && \
    apt-get install -y unzip && \
    apt-get install -y net-tools && \
    apt-get install -y iputils-ping && \
    apt-get install -y lib32gcc1 && \
    apt-get install -y lib32z1 && \
    apt-get install -y lib32stdc++6

# Download and extract the dedicated server files
WORKDIR /root
RUN wget https://download.kronos.gg/brink-dedicated-server.zip && \
    unzip brink-dedicated-server.zip -d brink-dedicated-server

# Set the working directory to the dedicated server folder
WORKDIR /root/brink-dedicated-server

# Set the server configuration
RUN echo "seta si_name \"$SERVER_NAME\"" >> brinkserver.cfg && \
    echo "seta net_serverPort \"$SERVER_PORT\"" >> brinkserver.cfg && \
    echo "seta net_serverPortMaster \"$SERVER_PORT_MASTER\"" >> brinkserver.cfg && \
    echo "seta g_password \"$SERVER_PASSWORD\"" >> brinkserver.cfg && \
    echo "seta si_maxPlayers \"$MAX_PLAYERS\"" >> brinkserver.cfg && \
    echo "seta si_minPlayers \"$MIN_PLAYERS\"" >> brinkserver.cfg && \
    echo "seta si_maxTeamSize \"$MAX_TEAM_SIZE\"" >> brinkserver.cfg && \
    echo "seta si_teamDamage \"$TEAM_DAMAGE\"" >> brinkserver.cfg && \
    echo "seta si_rankRestricted \"$RANK_RESTRICTIONS\"" >> brinkserver.cfg && \
    echo "seta g_mapRotationFixed \"$MAP_ROTATION\"" >> brinkserver.cfg && \
    echo "seta si_fixedMapDuration \"$FIXED_MAP_DURATION\"" >> brinkserver.cfg && \
    echo "seta si_overtime \"$OVERTIME\"" >> brinkserver.cfg && \
    echo "seta si_globalVoiceChat \"$GLOBAL_VOIP\"" >> brinkserver.cfg && \
    echo "seta si_teamVoiceChat \"$TEAM_VOIP\"" >> brinkserver.cfg && \
    echo "seta si_botDifficulty \"$BOTS\"" >> brinkserver.cfg && \
    echo "seta g_disableVoting \"$VOTING\"" >> brinkserver.cfg && \
    echo "exec $RULES_CONFIG" >> brinkserver.cfg

# Expose the server ports
EXPOSE $SERVER_PORT/tcp $SERVER_PORT/udp $SERVER_PORT_MASTER/tcp $SERVER_PORT_MASTER/udp

# Start the server
CMD ["./brinkded.x86", "+exec", "brinkserver.cfg"]
